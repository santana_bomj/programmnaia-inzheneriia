import * as types from './actionTypes';

const initialState = {
    images: [],
    activeTool: '',
    means: [],
    resultMeans: [],
    congifTools: [
        {
            name: 'Wwwc',
            mode: 'active',
            modeOptions: { mouseButtonMask: 1 },
        },
        {
            name: 'Zoom',
            mode: 'active',
            modeOptions: { mouseButtonMask: 2 },
        },
        {
            name: 'Pan',
            mode: 'active',
            modeOptions: { mouseButtonMask: 4 },
        },
        { name: 'StackScrollMouseWheel', mode: 'active' },
        { name: 'PanMultiTouch', mode: 'active' },
        { name: 'ZoomTouchPinch', mode: 'active' }, { name: 'StackScrollMultiTouch', mode: 'active' },
        'Length',
        'Angle',
        'Bidirectional',
        'FreehandRoi',
        'Eraser',
    ]
};

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SET_ACTIVE_TOOL:
            return { ...state, activeTool: action.value }
        case types.APPEND_URL:
            return { ...state, images: [...state.images, action.value] }
        case types.APPEND_MEAN:
            return { ...state, means: [...state.means, action.value] }
        case types.SET_RESULT_MEAN:
            return { ...state, resultMeans: action.value }
        default:
            return state;
    }
}

export function getImages(state) {
    return state.cornerstone.images
}
export function getActiveTool(state) {
    return state.cornerstone.activeTool
}
export function getMeans(state) {
    return state.cornerstone.means
}
export function getResultMeans(state) {
    return state.cornerstone.resultMeans
}
export function getConfigTools(state) {
    return state.cornerstone.congifTools
}