export const SET_ACTIVE_TOOL = 'cornerstone.SET_ACTIVE_TOOL'
export const APPEND_URL = 'cornerstone.APPEND_URL'
export const APPEND_MEAN = 'cornerstone.APPEND_MEAN'
export const SET_RESULT_MEAN = 'cornerstone.SET_RESULT_MEAN'