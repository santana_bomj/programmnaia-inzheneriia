import * as types from './actionTypes';
import { actions } from 'redux-router5'
import * as cornerstoneWADOImageLoader from "cornerstone-wado-image-loader";

export function handleTools(value) {
    return async (dispatch, getState) => {
        try {
            dispatch({ type: types.SET_ACTIVE_TOOL, value });
        } catch (error) {
        }
    };
}

export function onSelectFile(e) {
    return async (dispatch, getState) => {
        try {
            if (e.target.files && e.target.files.length > 0) {
                let fd = new FormData()
                Array.from(e.target.files).forEach(file => {
                    fd.append("file", file)
                    let imageId = cornerstoneWADOImageLoader.wadouri.fileManager.add(file)
                    cornerstoneWADOImageLoader.external.cornerstone.loadImage(imageId).then((image) => {
                        dispatch({ type: types.APPEND_URL, value: image.imageId });
                        if (getState().cornerstone.images.length === 1)
                            dispatch(actions.navigateTo('ct'))
                    })
                });
            }
        }
        catch (error) {
        }
    }
}

export function onActivate() {
    return async (dispatch, getState) => {
        try {
            if (getState().cornerstone.images.length === 0)
                dispatch(actions.navigateTo('home', {}, { replace: true }))
        } catch (error) {
        }
    };
}