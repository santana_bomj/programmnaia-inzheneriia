import cornerstone from './cornerstone/reducer'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { router5Middleware, router5Reducer } from 'redux-router5'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import onActivateMiddleware from '../middleware/onActivate'
import routes from '../routes';

export default function configureStore(router, initialState = {}) {
    const createStoreWithMiddleware = applyMiddleware(
        thunk,
        router5Middleware(router),
        onActivateMiddleware(routes),
        createLogger()
    )(createStore)

    const store = createStoreWithMiddleware(
        combineReducers({
            router: router5Reducer,
            cornerstone
        }),
        initialState
    )
    return store
}