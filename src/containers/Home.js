import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as cornerstoneActions from '../store/cornerstone/actions';

class Home extends Component {
    render() {
        return (
            <>
                <div className='Home'>
                    <div className='Home__title'>Просмотр DICOM</div>
                    <div className='Home__subtitle'>DICOM – это стандартный формат файла для медицинских изображений</div>
                    <div>Выберите папку с файлами формата dcm</div>
                    <div className='Home__select-file'><input directory="" webkitdirectory="" type="file" onChange={this.props.onSelectFile} /><span>Выберите папку</span></div>
                </div>
            </>
        )
    }
}

function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onSelectFile: (e) => dispatch(cornerstoneActions.onSelectFile(e))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);