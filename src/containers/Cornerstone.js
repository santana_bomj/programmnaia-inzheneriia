import React, { Component } from 'react';
import { connect } from 'react-redux';
import CornerstoneViewport from 'react-cornerstone-viewport'

import * as cornerstoneSelectors from '../store/cornerstone/reducer';
import * as cornerstoneActions from '../store/cornerstone/actions';

class Cornerstone extends Component {
    render() {
        return (
            <>
                {this.props.images.length > 0 ? <>
                    <CornerstoneViewport
                        tools={this.props.congifTools}
                        imageIds={this.props.images}
                        style={{ minWidth: '100%', height: '512px', flex: '1' }}
                        activeTool={this.props.activeTool}
                    />
                    <div className='Select'>
                        <select
                            value={this.props.activeTool}
                            onChange={evt =>
                                this.props.handleTools(evt.target.value)
                            }
                            placeholder='Выберите инструмент'
                        >
                            <option value="Wwwc">Wwwc</option>
                            <option value="Zoom">Zoom</option>
                            <option value="Pan">Pan</option>
                            <option value="Length">Length</option>
                            <option value="Angle">Angle</option>
                            <option value="Bidirectional">Bidirectional</option>
                            <option value="FreehandRoi">Freehand</option>
                            <option value="Eraser">Eraser</option>
                        </select>
                    </div>
                </> : <div>Не найдено dicom</div>}
            </>
        )
    }
}

function mapStateToProps(state) {
    return {
        images: cornerstoneSelectors.getImages(state),
        activeTool: cornerstoneSelectors.getActiveTool(state),
        congifTools: cornerstoneSelectors.getConfigTools(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        handleTools: (value) => dispatch(cornerstoneActions.handleTools(value)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cornerstone);