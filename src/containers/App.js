import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RouteNode } from 'react-router5'

import Container from './Container';
import Home from './Home';
import Cornerstone from './Cornerstone';

import './style.scss'

class App extends Component {

    render() {
        let activePanel
        switch (this.props.route.name) {
            case 'home':
                activePanel = <Home router={this.props.router} />
                break;
            case 'ct':
                activePanel = <Cornerstone router={this.props.router} />
                break;
            default:
                activePanel = '404'
                break;
        }

        return (
            <div>
                <Container>
                    {activePanel}
                </Container>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    (props) => (
        <RouteNode nodeName="">
            {({ route }) => <App route={route} {...props} />}
        </RouteNode>
    )
);
