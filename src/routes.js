import * as cornerstoneActions from './store/cornerstone/actions';

export default [
    { name: 'home', path: '/' },
    {
        name: 'ct', path: '/ct', onActivate: dispatch => params => {
            dispatch(cornerstoneActions.onActivate())
        }
    }
]